# ![La Salle BES](http://jcarreras.es/images/lasalle.png)

# ![screenshot](.screenshot.gif)

# Descripción
-----------------------

Funcionamiento básico del negociador de mensajes RabbitMQ

Ejemplo de un productor de mensajes y un consumidor de los mismos.


# Instalación
-----------------------

```
$ vagrant up
```


# Instrucciones
-----------------------

- Entra en la máquina por ssh con `vagrant ssh`
- Entra en el directorio src y ejecuta `composer update`
- Lee el contenido de los dos ficheros: `producer.php` y `worker.php`
- Ejecuta `php producer.php`, para encolar un mensaje
- Ejecuta `php worker.php` para dejar encendido el consumidor de tareas. Debería haber consumido la que acabamos de crear.
- Prueba de ejecutar repetidas veces el productor de mensajes para ver que se consumen por el worker.


# Desinstalación
-----------------------

```
$ vagrant destroy
```
