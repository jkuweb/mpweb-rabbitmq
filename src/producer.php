<?php

require_once __DIR__ . '/vendor/autoload.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

// Nos conectamos a RabbitMQ
$connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');

// Creamos un nuevo channel
$channel    = $connection->channel();

// Asignamos la cola 'email', donde escucharemos. Si no estuviese creada, se crearía ahora,
// si ya está creada solo se asignaría al channel
$channel->queue_declare('email', false, false, false, false);

// Creamos un nuevo mensaje, que será recibido por cualquier worker activo, o será almacenada en RabbitMQ
$msg = new AMQPMessage('Hello World!');

// Publicamos el mensaje en la cola 'email'
$channel->basic_publish($msg, '', 'email');

// Imprimimos por pantalla que hemos mandado el mensaje
echo " [x] Sent 'Hello World!'\n";