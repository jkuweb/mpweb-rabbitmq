<?php

require_once __DIR__ . '/vendor/autoload.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;

// Nos conectamos a RabbitMQ
$connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');

// Creamos un nuevo channel
$channel    = $connection->channel();

// Y asignamos la cola 'email', donde escucharemos. Si no estuviese creada, se crearía ahora,
// si ya está creada solo se asignaría al channel
$channel->queue_declare('email', false, false, false, false);

echo ' [*] Waiting for messages. To exit press CTRL+C', "\n";


/*$callback = function ($msg) {
	echo " [x] Received ", $msg->body, "\n";
};*/

// Definirmos un consumidor, que escuchará en la cola de EMAIL, y cuando reciba un mensaje será procesado por
// la función definida como último parámetro
$channel->basic_consume('email', '', false, true, false, false, function ($msg) {
	// En este caso simplemente imprimimos por pantalla el mensaje que se haya publicado
	echo " [x] Received ", $msg->body, "\n";
});

// Éste es el modo que tenemos para esperar a la llegada de mensajes...
while (count($channel->callbacks)) {
	// ... mientras esperamos un mensaje,e speramos.
	$channel->wait();
}